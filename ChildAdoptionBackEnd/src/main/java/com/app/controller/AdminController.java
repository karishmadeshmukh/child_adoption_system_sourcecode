package com.app.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Adoptee;
import com.app.pojos.ChildEntity;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;
import com.app.service.IAdminService;

@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {
	//dependency : admin service layer
	@Autowired
	@Lazy
	private IAdminService adminService;
		
	public AdminController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
	//add request handling method to login admin
	@PostMapping("/login")
	public ResponseEntity<?> authenticateAdmin(@RequestBody UserAuthInfo user) {
		System.out.println("Inside authenticateAdmin(), admin details : " + user);	//TRANSIENT
		//return adminService.authenticateAdmin(user);
		return ResponseEntity.ok(adminService.authenticateAdmin(user));
	}
	
	//add request handling method to login admin
	@PutMapping("/changepassword")
	public String changePassword(@RequestBody User user) {
		System.out.println("Inside changePassword(), admin details : " + user);	//TRANSIENT
		return adminService.changePassword(user);
	}
	
	//add request handling method to confirm adoption and generate report
	@PutMapping("/report/{childId}")
	public String confirmAdoptionAndGenerateReport(@PathVariable int childId) {
		System.out.println("Inside confirmAdoptionAndGenerateReport(), childId : " + childId);	//TRANSIENT
		return adminService.generateReportAndAdoptionConfirmation(childId);
	}
	
	//add method to view adoptees
	@GetMapping("/viewadoptees")
	public ResponseEntity<?> viewAdoptees(){
		System.out.println("inside viewAdoptees()");
		List<Adoptee> adoptees = adminService.getAllAdoptees();
		if(adoptees.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(adoptees, HttpStatus.OK);
	}
	
	//add method to view donors
	@GetMapping("/viewdonors")
	public ResponseEntity<?> viewDonors(){
		System.out.println("inside viewDonors()");
		List<Donor> donors = adminService.getAllDonors();
		if(donors.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(donors, HttpStatus.OK);
	}
	
	//add method to view children
	@GetMapping("/viewchildren")
	public ResponseEntity<?> viewChildren(){
		System.out.println("inside viewChildren()");
		List<Children> children = adminService.getAllChildren();
		if(children.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(children, HttpStatus.OK);
	}
	
	//add method to view children
	@GetMapping("/viewadoptedchildren")
	public ResponseEntity<?> viewAdoptedChildren(){
		System.out.println("inside viewChildren()");
		List<Children> children = adminService.getAdoptedChildren();
		if(children.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(children, HttpStatus.OK);
	}
	
	//add method to view healthStatus of all childrens
	@GetMapping("/viewhealthStatus")
	public List<HealthStatus> viewChildrenHealthStatus(){
		System.out.println("inside viewChildrenHealthStatus()");
		return adminService.getHealthStatusOfAllChildren();
		
	}
	
	//add method to view healthStatus of all childrens
	@GetMapping("/viewedu")
	public List<EducationalDetails> viewChildrenEducationalDetails(){
		System.out.println("inside viewChildrenEducationalDetails()");
		return adminService.getEducationalDEtailsOfAllChildren();
	}
	
	
	//add method to view child profile
	@GetMapping("/childprofile/{childId}")
	public Children viewChildProfile(@PathVariable int childId) {
		return adminService.viewChildProfile(childId);
	}

	//add method to view donor profile
	@GetMapping("/donorprofile/{donorId}")
	public Donor viewDonorProfile(@PathVariable int donorId) {
		return adminService.viewDonorProfile(donorId);
	}

	//add method to view adoptee profile
	@GetMapping("/adopteeprofile/{adopteeId}")
	public Adoptee viewAdopteeProfile(@PathVariable int adopteeId) {
		return adminService.viewAdopteeProfile(adopteeId);
	}
	
	//add method to view healthStatus of all childrens
	@GetMapping("/childrentest")
	public ResponseEntity<?> viewChildrenDetailsTest(){
		System.out.println("inside viewChildrenDetailsTest()");
		ChildEntity entity = new ChildEntity("MyName", LocalDate.now(), 18, "F", "Blank", false, false, 50.1f, 5.5f, "O+", 9.5f, "Stable", "Stable", "SSC","XYZ");
		ChildEntity entity1 = new ChildEntity("MyName", LocalDate.now(), 18, "F", "Blank", false, false, 50.1f, 5.5f, "O+", 9.5f, "Stable", "Stable", "SSC","XYZ");
		ChildEntity entity2 = new ChildEntity("MyName", LocalDate.now(), 18, "F", "Blank", false, false, 50.1f, 5.5f, "O+", 9.5f, "Stable", "Stable", "SSC","XYZ");
		ChildEntity entity3 = new ChildEntity("MyName", LocalDate.now(), 18, "F", "Blank", false, false, 50.1f, 5.5f, "O+", 9.5f, "Stable", "Stable", "SSC","XYZ");
		List<ChildEntity> childEntities = new ArrayList<>();
		childEntities.add(entity);
		childEntities.add(entity1);
		childEntities.add(entity2);
		childEntities.add(entity3);
		if(childEntities.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(childEntities, HttpStatus.OK);
	}
}
