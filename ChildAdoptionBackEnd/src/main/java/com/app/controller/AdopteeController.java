package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Adoptee;
import com.app.pojos.Child;
import com.app.pojos.Children;
import com.app.pojos.User;
import com.app.service.IAdopteeService;

@CrossOrigin
@RestController
@RequestMapping("/adoptee")
public class AdopteeController {
	//dependency : adoptee service layer
	@Autowired
	@Lazy
	private IAdopteeService adopteeService;
		
	public AdopteeController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
	//add request handling method to process adoptee's registration
	@PostMapping("/register")
	public ResponseEntity<?> processAdopteeRegistrationForm(@RequestBody Adoptee adoptee) {
		System.out.println("Inside processAdopteeRegistrationForm(), adoptee details : " + adoptee);	//TRANSIENT
		return ResponseEntity.ok(adopteeService.registerAdoptee(adoptee));
	}
	
	//add request handling method to authenticate adoptee
	@PostMapping("/login")
	public ResponseEntity<?> authenticateAdoptee(@RequestBody User user) {
		System.out.println("Inside authenticateAdoptee(), adoptee details : " + user.getEmail());	//TRANSIENT
		return ResponseEntity.ok(adopteeService.authenticateAdoptee(user));
	}
	
	//add request handling method to change adoptee's password
	@PutMapping("/changepassword")
	public String changepassword(@RequestBody User user) throws Exception {
		System.out.println("Inside changePassword(), donor details : " + user);	//TRANSIENT
		return adopteeService.changePassword(user);
	}
	
	//add request handling method to search children
	@GetMapping("/search")
	public List<Children> getChildrenList() {
		System.out.println("Inside searchChildren()");
		return adopteeService.getChildren();
	}
	
	//add request handling method to view child profile
	@GetMapping("/childProfile/{childId}")
	public Children viewChildProfile(@PathVariable int childId) throws Exception {
		System.out.println("Inside viewChildProfile(), childId : " + childId);	//TRANSIENT
		return adopteeService.viewChildProfile(childId);
	}
	
	//add method to request for child
	@GetMapping("/requestchild")
	public ResponseEntity<?>  requestForChild(@RequestParam int adopteeid, @RequestParam int childId) {
		System.out.println("Inside requestForChild(), childId : " + childId + " ,adopteeId : " + adopteeid);	//TRANSIENT
		return ResponseEntity.ok(adopteeService.sendRequest(childId, adopteeid));
    }
	
	//add request handling method to confirm request
	@GetMapping("/confirmrequest/{childId}")
	public ResponseEntity<?> confirmRequest(@PathVariable int childId){
		System.out.println("Inside confirmRequest() of DonorController");
		return ResponseEntity.ok(adopteeService.confirmRequest(childId));
	}
	
	
	//add a method to get Requested child  by specific adoptee
	@GetMapping("/requestedchildren/{adopteeid}")
	public ResponseEntity<?> getRequestedChildren(@PathVariable int adopteeid){
		System.out.println("Inside getRequestedChildren(), adopteeid : " + adopteeid);
		return ResponseEntity.ok(adopteeService.getRequestedChildren(adopteeid));
	}
	
	//add a method to get adopted children by adoptee id
	@GetMapping("/adoptedchildrenbyadopteeid/{adopteeid}")
	public ResponseEntity<?> getAdoptedChildrenByAdopteeId(@PathVariable int adopteeid){
		System.out.println("Inside getAdoptedChildrenByAdopteeId(), adopteeId : " + adopteeid);
		return ResponseEntity.ok(adopteeService.getAdoptedChildrenByAdopteeId(adopteeid));
	}
}
