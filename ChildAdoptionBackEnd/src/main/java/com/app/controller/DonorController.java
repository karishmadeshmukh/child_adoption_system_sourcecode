package com.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.ChildEntity;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;
import com.app.service.IDonorService;

@CrossOrigin
@RestController
@RequestMapping("/donor")
public class DonorController {
	//dependency : donor service layer
	@Autowired
	@Lazy
	private IDonorService donorService;
		
	public DonorController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
	//add request handling method to confirm request
	@GetMapping("/acceptrequest/{childId}")
	public ResponseEntity<?> acceptRequest(@PathVariable int childId){
		System.out.println("Inside confirmRequest() of DonorController");
		return ResponseEntity.ok(donorService.acceptRequest(childId));
	}
	
	//add request handling method to get donated children
	@GetMapping("/donatedchildren/{donorId}")
	public ResponseEntity<?> getDonatedChildrenList(@PathVariable int donorId){
		System.out.println("Inside getDonatedChildrenList() of DonorController");
		List<Children> donatedChildren = donorService.getDonatedChildrenList(donorId);
		return new ResponseEntity<>(donatedChildren, HttpStatus.OK);
	}
	
	//add request handling method to process child's registration form
	@PostMapping("/registerchild")
	public String processChildRegistrationForm(@RequestBody Children child) {
		System.out.println("Inside processChildRegistrationForm(), child details : " + child);	//TRANSIENT
		System.out.println("Child details : " + child);
		System.out.println("health details : " + child.getHealthStatus());
		System.out.println("educational details : " + child.getEducationalDetails());
		return donorService.registerNewChildren(child);
	}
	
	//add request handling method to process donor's registration form
	@PostMapping("/registerdonor")
	public ResponseEntity<?> processDonorRegistrationForm(@RequestBody Donor donor) {
		System.out.println("Inside processDonorRegistrationForm(), donor details : " + donor);	//TRANSIENT
		System.out.println("Donor details : " + donor);
		return ResponseEntity.ok(donorService.registerDonor(donor));
	}
	
	//add request handling method to login donor
	@PostMapping("/login")
	public ResponseEntity<?> authenticateDonor(@RequestBody UserAuthInfo user) {
		System.out.println("Inside authenticateDonor(), donor details : " + user.getEmail());	//TRANSIENT
		return ResponseEntity.ok(donorService.authenticateDonor(user));
	}
	
	//add request handling method to change donor's password
	@PutMapping("/changepassword")
	public String changepassword(@RequestBody User user) throws Exception {
		System.out.println("Inside changePassword(), donor details : " + user);	//TRANSIENT
		return donorService.changePassword(user);
	}
	
	//add request handling method to process child's registration form
	@PostMapping("/registerchildtry/{donorId}")
	public ResponseEntity<?> processChildRegistrationFormTry(@RequestBody ChildEntity entity, @PathVariable int donorId) {
		System.out.println("Inside processChildRegistrationFormTRy(), entity details : " + entity);	//TRANSIENT
		System.out.println("donorId : " + donorId);
		Children children = new Children();
		children.setName(entity.getName());
		children.setBirthDate(entity.getBirthDate());
		children.setAge(entity.getAge());
		children.setGender(entity.getGender());
		children.setImage(entity.getImage());
		children.setAdopted(entity.isAdopted());
		children.setReport(entity.isReport());
		
		HealthStatus healthStatus = new HealthStatus();
		healthStatus.setWeight(entity.getWeight());
		healthStatus.setHeight(entity.getHeight());
		healthStatus.setBloodGroup(entity.getBloodGroup());
		healthStatus.setHb(entity.getHb());
		healthStatus.setMentalHealthStatus(entity.getMentalHealthStatus());
		healthStatus.setPhysicalHealthStatus(entity.getPhysicalHealthStatus());
		
		EducationalDetails educationalDetails = new EducationalDetails();
		educationalDetails.setDegree(entity.getDegree());
		educationalDetails.setInstitute(entity.getInstitute());
		
		System.out.println("children : " + children);
		System.out.println("health status : " + healthStatus);
		System.out.println("educational details : " + educationalDetails);
		
		healthStatus.setChildren(children);
		educationalDetails.setChildren(children);
		
		children.setHealthStatus(healthStatus);
		children.setEducationalDetails(educationalDetails);
		
		System.out.println("children : " + children);
		
		return ResponseEntity.ok(donorService.addNewChildren(children, donorId));
	}
}
