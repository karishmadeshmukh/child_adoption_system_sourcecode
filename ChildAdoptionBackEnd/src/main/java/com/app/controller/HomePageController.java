package com.app.controller;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
//@Controller //mandatory
@RequestMapping("/home")
public class HomePageController {
	public HomePageController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	//add a req handling method to return home page (index page)
	@RequestMapping("/")
	public String showHomePage(Model modelAttrMap)
	{
		System.out.println("in show home page "+modelAttrMap);
		modelAttrMap.addAttribute("server_time", new Date());
		return "/index";
	}
}
