package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.app.dao.IChildrenDao;
import com.app.dao.IDonorDao;
import com.app.pojos.ChildEntity;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

@Service //Mandatory annotation to tell SC whatever follows contains B.L
@Transactional//Mandatory : annotation to tell SC , to use tx mgr bean for
//automatically handling txs.
public class DonorServiceImpl implements IDonorService{
	//dependency : DAO layer
	@Autowired
	private IChildrenDao childrenDao;
	
	@Autowired
	private IDonorDao donorDao;
	
	@Override
	public String registerNewChildren(Children children) {
		children.getHealthStatus().setChildren(children);
		children.getEducationalDetails().setChildren(children);
		//Donor donor = donorDao.findById(donorId);
		//children.setDonor(donor);
		return childrenDao.registerNewChildren(children);
		//JPA implementor(Hibernate) auto dirty checking : insert : entityManager : closed
	}

	@Override
	public Donor registerDonor(Donor donor) {
		return donorDao.registerDonor(donor);
	}

	@Override
	public Donor authenticateDonor(UserAuthInfo user) {
		return donorDao.authenticateDonor(user);
	}

	@Override
	public String changePassword(User user) {
		return donorDao.changePassword(user);
	}

	@Override
	public ChildEntity addNewChildren(Children children, int donorId) {
		Donor donor = null;
		donor = donorDao.findById(donorId);
		children.setDonor(donor);
		return childrenDao.addNewChildren(children);
	}

	@Override
	public List<Children> getDonatedChildrenList(int donorId) {
		System.out.println("Inside getDonatedChildrenList() of DonorServiceImpl");
		return childrenDao.getDonatedChildrenList(donorId);
	}

	@Override
	public Children acceptRequest(int childId) {
		return childrenDao.acceptRequest(childId);
	}

}
