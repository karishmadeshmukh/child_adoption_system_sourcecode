package com.app.service;

import java.util.List;

import com.app.pojos.Adoptee;
import com.app.pojos.Child;
import com.app.pojos.Children;
import com.app.pojos.User;

public interface IAdopteeService {
	//add method to register adoptee
	Adoptee registerAdoptee(Adoptee adoptee);
	
	//add method to authenticate adoptee
	Adoptee authenticateAdoptee(User user);
	
	//add method to change password
	String changePassword(User user);
	
	//add method to search children
	List<Children> getChildren();
	
	//add method to send request to a child
	Children sendRequest(int childId, int adopteeId);
	
	//add method to view child profile
	Children viewChildProfile(int childId);
	
	//add method to confirm adoption
	Children confirmAdoption(int childId);
	
	//add method to get requested children by specific adoptee
	List<Children> getRequestedChildren(int adopteeid);
	
	//add me thod to confirm request
	Children confirmRequest(int childId);
	
	//add method to get adopted children by adoptee id
	List<Children> getAdoptedChildrenByAdopteeId(int adopteeid);
}
