package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IAdminDao;
import com.app.dao.IAdopteeDao;
import com.app.dao.IChildrenDao;
import com.app.dao.IDonorDao;
import com.app.dao.IEducationalDetailsDao;
import com.app.dao.IHealthStatusDao;
import com.app.pojos.Adoptee;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

@Service //Mandatory annotation to tell SC whatever follows contains B.L
@Transactional//Mandatory : annotation to tell SC , to use tx mgr bean for
//automatically handling txs.
public class AdminServiceImpl implements IAdminService {
	//dependency : DAO layer
	@Autowired
	private IAdminDao adminDao;
	
	@Autowired
	private IAdopteeDao adopteeDao;
	
	@Autowired
	private IDonorDao donorDao;
	
	@Autowired
	private IChildrenDao childrenDao;
	
	@Autowired
	private IHealthStatusDao healthStatusDao;
	
	@Autowired
	private IEducationalDetailsDao educationalDetailsDao;

	@Override
	public UserAuthInfo authenticateAdmin(UserAuthInfo user) {
		return adminDao.authenticateAdmin(user);
	}

	@Override
	public String changePassword(User user) {
		return adminDao.changePassword(user);
	}

	@Override
	public String generateReportAndAdoptionConfirmation(int childId) {
		return adminDao.generateReportAndAdoptionConfirmation(childId);
	}

	@Override
	public List<Adoptee> getAllAdoptees() {
		return adopteeDao.getAllAdoptees();
	}

	@Override
	public List<Donor> getAllDonors() {
		return donorDao.getAllDonors();
	}

	@Override
	public List<Children> getAllChildren() {
		return childrenDao.getAllChildren();
	}

	@Override
	public List<HealthStatus> getHealthStatusOfAllChildren() {
		return healthStatusDao.getHealthStatusOfAllChildren();
	}

	@Override
	public List<EducationalDetails> getEducationalDEtailsOfAllChildren() {
		return educationalDetailsDao.getEducationalDEtailsOfAllChildren();
	}

	@Override
	public Children viewChildProfile(int childId) {
		return childrenDao.findById(childId);
	}

	@Override
	public Donor viewDonorProfile(int donorId) {
		return donorDao.findById(donorId);
	}

	@Override
	public Adoptee viewAdopteeProfile(int adopteeId) {
		return adopteeDao.findById(adopteeId);
	}

	@Override
	public List<Children> getAdoptedChildren() {
		return childrenDao.getAdoptedChildren();
	}
	 
}
