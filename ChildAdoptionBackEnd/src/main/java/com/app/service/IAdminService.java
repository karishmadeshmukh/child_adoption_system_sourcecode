package com.app.service;

import java.util.List;

import com.app.pojos.Adoptee;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

public interface IAdminService {
	//add a method to authenticate admin
	UserAuthInfo authenticateAdmin(UserAuthInfo user);
	
	//add a method to change admin's password
	String changePassword(User user);
	
	//add method to generate report and adoption confirmation
	String generateReportAndAdoptionConfirmation(int childId);
	
	//add method to get all adoptees
	List<Adoptee> getAllAdoptees();
	
	//add method to get all donors
	List<Donor> getAllDonors();
		
	//add a method to getAllChildren
	List<Children> getAllChildren();
	
	//add method to get health status of all children
	List<HealthStatus> getHealthStatusOfAllChildren();
	
	//add method to get educational details of all children
	List<EducationalDetails> getEducationalDEtailsOfAllChildren();
	
	//add method to view child's profile
	Children viewChildProfile(int childId);
	
	//add method to view donor's profile
	Donor viewDonorProfile(int donorId);
		
	//add method to view adoptee's profile
	Adoptee viewAdopteeProfile(int adopteeId);
	
	//add method to get all adopted children
	List<Children> getAdoptedChildren();
}
