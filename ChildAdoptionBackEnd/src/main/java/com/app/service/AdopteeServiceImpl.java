package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IAdopteeDao;
import com.app.dao.IChildrenDao;
import com.app.pojos.Adoptee;
import com.app.pojos.Child;
import com.app.pojos.Children;
import com.app.pojos.User;

@Service //Mandatory annotation to tell SC whatever follows contains B.L
@Transactional//Mandatory : annotation to tell SC , to use tx mgr bean for
//automatically handling txs.
public class AdopteeServiceImpl implements IAdopteeService {
	//dependency : DAO layer
	@Autowired
	private IAdopteeDao adopteeDao;

	@Override
	public Adoptee registerAdoptee(Adoptee adoptee) {
		return adopteeDao.registerAdoptee(adoptee);
	}

	@Override
	public Adoptee authenticateAdoptee(User user) {
		return adopteeDao.authenticateAdoptee(user);
	}

	@Override
	public String changePassword(User user) {
		return adopteeDao.changePassword(user);
	}

	@Override
	public List<Children> getChildren() {
		return adopteeDao.getChildren();
	}

	@Override
	public Children sendRequest(int childId, int adopteeId) {
		return adopteeDao.sendRequest(childId, adopteeId);
	}

	@Override
	public Children viewChildProfile(int childId) {
		return adopteeDao.viewChildProfile(childId);
	}

	@Override
	public Children confirmAdoption(int childId) {
		return adopteeDao.confirmAdoption(childId);
	}

	@Override
	public List<Children> getRequestedChildren(int adopteeid) {
		return adopteeDao.getRequestedChildren(adopteeid);
	}

	@Override
	public Children confirmRequest(int childId) {
		return adopteeDao.confirmRequest(childId);
	}

	@Override
	public List<Children> getAdoptedChildrenByAdopteeId(int adopteeid) {
		return adopteeDao.getAdoptedChildrenByAdopteeId(adopteeid);
	}
}
