package com.app.service;

import java.util.List;

import com.app.pojos.ChildEntity;
import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

public interface IDonorService {
	//add a method to register new children
	String registerNewChildren(Children children);
	
	//add method to register donor
	Donor registerDonor(Donor donor);
	
	//add method to authenticate donor
	Donor authenticateDonor(UserAuthInfo donor);
	
	//add method to change password
	String changePassword(User user);
	
	//add method to add new children
	ChildEntity addNewChildren(Children children, int donorId);
	
	//add method go get donated children
	List<Children> getDonatedChildrenList(int donorId);
	
	//add me thod to confirm request
	Children acceptRequest(int childId);
}
