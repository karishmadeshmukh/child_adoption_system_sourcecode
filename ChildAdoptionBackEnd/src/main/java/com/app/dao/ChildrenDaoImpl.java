package com.app.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Adoptee;
import com.app.pojos.ChildEntity;
import com.app.pojos.Children;
import com.app.pojos.Donor;

@Repository // spring bean : data access logic
public class ChildrenDaoImpl implements IChildrenDao{
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired 
	private AdopteeDaoImpl adopteeDao;
	
	@Autowired
	private DonorDaoImpl donorDao;

	@Override
	public String registerNewChildren(Children children) {
		String msg = "Child registration failed...";
		manager.persist(children);
		msg = "Child registered successfully...";
		return msg;
	}

	@Override
	public List<Children> getAllChildren() {
		String jpql = "select c from Children c";
		return manager.createQuery(jpql, Children.class).getResultList(); 
	}

	@Override
	public Children findById(int id) {
		Children child = null;
		String jpql = "select c from Children c where c.childId=:id";
		//String jpql = "select c from Children c join fetch c.healthStatus join fetch c.educationalDetails where c.childId=:id";
		child = manager.createQuery(jpql, Children.class).setParameter("id", id).getSingleResult();
		return child;
	}

	@Override
	public Children requestForChild(int childId, int adopteeId) {																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								
		Children child = findById(childId);
		Adoptee adoptee = adopteeDao.findById(adopteeId);
		child.setAdoptee(adoptee);
		child.setRequested(1);
		//
		manager.merge(child);
		return child;
	}

	@Override
	public String generateReportAndAdoptionConfirmation(int childId) {
		String msg = "Confirmation not done hence report is not generated";
		Children child = findById(childId);
		child.setAdopted(true);
		child.setReport(true);
		manager.merge(child);
		msg = "Adoption confirmed and report generated";
		return msg;
	}
	
	@Override
	public ChildEntity addNewChildren(Children children) {
		String msg = "Child registration failed...";
		String[] images = {"https://almustafa.pk/wp-content/uploads/2020/02/Orphan-MFL-330x230-min.jpg", 
							"https://3hjlwq2qgk694v4222xmfbvt-wpengine.netdna-ssl.com/wp-content/uploads/2011/11/child-alone.jpg", 
							"https://djfromtheorphanage.files.wordpress.com/2019/12/sadgirl.jpeg?w=636", 
							"https://i2.wp.com/www.dailysquat.com/wp-content/uploads/2019/09/sadchild.jpg?w=620&ssl=1",
							"https://mycrazyadoption.com/images/169.jpg", 
							"https://www.opendooradoption.org/wp-content/uploads/2016/12/bulgaria-300x226.jpg", 
							"https://mycrazyadoption.org/images/1314-333x333.jpg",
							"https://images.agoramedia.com/wte3.0/gcms/Toddler-Regression-of-Progression-article.jpg?width=574", 
							"https://www.sunsationalswimschool.com/assets/blog/meltdown_dilemmas/how-to-deal-with-toddler-tantrums-swimming-lessons.jpg", 
							"https://images.milledcdn.com/2020-09-26/Ls380L-oBcwhUOmZ/GdDBB28Vf0hv.jpeg",
							"https://www.tochildrenwithlove.ie/wp-content/uploads/2014/12/Indo-620x423.jpg",
							"https://amandacurcio.files.wordpress.com/2014/12/lost-child.jpg?w=772"
							};
		
		Random rand = new Random();
		int max = 10, min = 0;
		int randomNum = rand.nextInt((max - min) + 1) + min;
		
		children.setImage(images[randomNum]);
		
		manager.persist(children);
		msg = "Child registered successfully...";
		ChildEntity entity = new ChildEntity();
		entity.setName(children.getName());
		entity.setBirthDate(children.getBirthDate());
		entity.setAge(children.getAge());
		entity.setGender(children.getGender());
		entity.setImage(children.getImage());
		entity.setAdopted(children.isAdopted());
		entity.setReport(children.isReport());
		entity.setWeight(children.getHealthStatus().getWeight());
		entity.setHeight(children.getHealthStatus().getHeight());
		entity.setBloodGroup(children.getHealthStatus().getBloodGroup());
		entity.setHb(children.getHealthStatus().getHb());
		entity.setMentalHealthStatus(children.getHealthStatus().getMentalHealthStatus());
		entity.setPhysicalHealthStatus(children.getHealthStatus().getPhysicalHealthStatus());
		entity.setDegree(children.getEducationalDetails().getDegree());
		entity.setInstitute(children.getEducationalDetails().getInstitute());
		return entity;
	}

	@Override
	public List<Children> getAdoptedChildren() {
		List<Children> adoptedChildren = new ArrayList<>();
		List<Children> children = this.getAllChildren();
		for (Children child : children) {
			if(child.getAdoptee() != null)
				adoptedChildren.add(child);
		}
		return adoptedChildren;
	}
	
	@Override
	public List<Children> getDonatedChildrenList(int donorId) {
		System.out.println("Inside getDonatedChildrenList() of ChildrenDaoImpl");
		List<Children> donatedChildren = new ArrayList<Children>();
		List<Children> children = this.getAllChildren();
		for (Children child : children) {
			System.out.println("Requested : " + child.getRequested());
			if(child.getDonor().getId() == donorId) {
				donatedChildren.add(child);
			}
		}
		return donatedChildren;
	}

	@Override
	public Children confirmAdoption(int childId) {
		Children children = this.findById(childId);
		children.setAdopted(true);
		children.setReport(true);
		children.setConfirmed(1);
		manager.merge(children);
		return children;
	}

	@Override
	public List<Children> getRequestedChildren(int adopteeid) {
		String jpql = "select c from Children c where c.adoptee.id=:id";
		List<Children> children = new ArrayList<Children>();
		children = manager.createQuery(jpql, Children.class).setParameter("id", adopteeid).getResultList();
		List<Children> requestedChildren = new ArrayList<Children>();
		for (Children child : children) {
			if(!child.isAdopted())
				requestedChildren.add(child);
		}
		return requestedChildren;
	}

	@Override
	public Children confirmRequest(int childId) {
		Children child = this.findById(childId);
		child.setAdopted(true);
		child.setConfirmed(1);
		child.setReport(true);
		manager.merge(child);
		return child;
	}

	@Override
	public Children acceptRequest(int childId) {
		Children child = this.findById(childId);
		child.setReqaccepted(1);
		manager.merge(child);
		return child;
	}

	@Override
	public List<Children> getAdoptedChildrenByAdopteeId(int adopteeid) {
		String jpql = "select c from Children c where c.adoptee.id=:id";
		List<Children> children = new ArrayList<Children>();
		children = manager.createQuery(jpql, Children.class).setParameter("id", adopteeid).getResultList();
		List<Children> adoptedChildren = new ArrayList<Children>();
		for (Children child : children) {
			if(child.isAdopted())
				adoptedChildren.add(child);
		}
		return adoptedChildren;
	}

}
