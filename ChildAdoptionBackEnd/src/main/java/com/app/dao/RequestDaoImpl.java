package com.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository //spring bean : data access logic
public class RequestDaoImpl implements IRequestDao {
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void registerRequest(int childId, int adopteeId) {
		//manager.persist(adoptee);
	}

}
