package com.app.dao;

import com.app.pojos.Children;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

public interface IAdminDao {
	//add a method to authenticate admin
	UserAuthInfo authenticateAdmin(UserAuthInfo user);
	
	//add a method to change admin's password
	String changePassword(User user);
	
	//add method to generate report and adoption confirmation
	String generateReportAndAdoptionConfirmation(int childId);
}
