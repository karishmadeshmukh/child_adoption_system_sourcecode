package com.app.dao;

import java.util.List;

import com.app.pojos.EducationalDetails;

public interface IEducationalDetailsDao {
	//add method to get educational details of all children
	List<EducationalDetails> getEducationalDEtailsOfAllChildren();
}
