package com.app.dao;

import java.util.List;

import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

public interface IDonorDao {
	//add method to register donor
	Donor registerDonor(Donor donor);
	
	//add method to authenticate donor
	Donor authenticateDonor(UserAuthInfo user);
	
	//add method to change password
	String changePassword(User user);
	
	//add method to find donor by id
	public Donor findById(int id);
	
	//add method to get all donors
	List<Donor> getAllDonors();
	
}
