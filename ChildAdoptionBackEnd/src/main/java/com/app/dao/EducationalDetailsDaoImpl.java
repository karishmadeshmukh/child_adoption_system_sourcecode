package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;

@Repository //spring bean : data access logic
public class EducationalDetailsDaoImpl implements IEducationalDetailsDao{
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<EducationalDetails> getEducationalDEtailsOfAllChildren() {
		String jpql = "select e from EducationalDetails e";
		return manager.createQuery(jpql, EducationalDetails.class).getResultList(); 
	}

}
