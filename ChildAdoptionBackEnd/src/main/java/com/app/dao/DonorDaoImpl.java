package com.app.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;

import com.app.pojos.Children;
import com.app.pojos.Donor;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;
import com.app.utils.PasswordUtils;

@Repository //spring bean : data access logic
public class DonorDaoImpl implements IDonorDao{
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
    private JavaMailSender javaMailSender;

	@Override
	public Donor registerDonor(Donor donor) {
		String msg = "Donor's registration failed";
		
		//password encryption
		String saltPassword = PasswordUtils.getSalt(6);
		donor.setSaltPassword(saltPassword);
		String encryptPassword = PasswordUtils.generateSecurePassword(donor.getPassword(), saltPassword);
		donor.setPassword(encryptPassword);
				
		manager.persist(donor);
		msg = "Donor registered successfully";
		sendEmail(donor.getEmail(), "Confirmation of registration", "Welcome to Child Adoption Portal\nYou have registered successfully as a Donor");
		return donor;
	}
	
	//@Autowired
    //private JavaMailSender javaMailSender;
    void sendEmail(String email, String subject, String body) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);
        msg.setSubject(subject);
        msg.setText(body);
        javaMailSender.send(msg);
    }

	@Override
	public Donor authenticateDonor(UserAuthInfo user) {
		String msg = "Donor login failed";
		Donor d = null;
		
		//String jpql = "select d from Donor d  where d.email=:email and d.password=:pass";
		String jpql = "select d from Donor d  where d.email=:email";

		d = manager.createQuery(jpql, Donor.class).setParameter("email", user.getEmail()).getSingleResult();
		
		//password encryption
		String saltPassword = d.getSaltPassword();
		String encryptPassword = PasswordUtils.generateSecurePassword(user.getPassword(), saltPassword);
		
		if((d.getEmail().equals(user.getEmail())) && (d.getPassword().equals(encryptPassword))) 
			msg = "Donor logged in successfully";
		System.out.println(msg);
		return d;
	}

	@Override
	public String changePassword(User user) {
		String msg = "Password updation failed";
		Donor donor = findById(user.getUserId());
		if(donor != null) {
			donor.setPassword(user.getPassword());
			manager.merge(donor);
			msg = "password updated successfully";
		}
		return msg;
	}

	@Override
	public Donor findById(int id) {
		Donor d = null;
		String jpql = "select d from Donor d  where d.id=:id";
		d = manager.createQuery(jpql, Donor.class).setParameter("id", id).getSingleResult();
		return d;
	}

	@Override
	public List<Donor> getAllDonors() {
		String jpql = "select d from Donor d";
		return manager.createQuery(jpql, Donor.class).getResultList(); 
	}
	
	
}
