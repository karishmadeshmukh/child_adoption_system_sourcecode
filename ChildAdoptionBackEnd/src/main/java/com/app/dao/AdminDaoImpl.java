package com.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Adoptee;
import com.app.pojos.User;
import com.app.pojos.UserAuthInfo;

@Repository // spring bean : data access logic
public class AdminDaoImpl implements IAdminDao {
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private IChildrenDao childrenDao;

	@Override
	public UserAuthInfo authenticateAdmin(UserAuthInfo user) {
		//String msg = "Admin login failed";
		User u = null;
		UserAuthInfo userAuthInfo = new UserAuthInfo();
		String jpql = "select u from User u  where u.email=:email and u.password=:pass";

		u = manager.createQuery(jpql, User.class).setParameter("email", user.getEmail())
				.setParameter("pass", user.getPassword()).getSingleResult();
		
		if((u.getEmail().equals(user.getEmail())) && (u.getPassword().equals(user.getPassword()))) {
			//msg = "Admin logged in successfully";
			userAuthInfo.setEmail(u.getEmail());
			userAuthInfo.setPassword(u.getPassword());
			
		}
		return userAuthInfo;
	}
	
	User findById(int id) {
		User admin = null;
		String jpql = "select u from User u  where u.userId=:id";

		admin = manager.createQuery(jpql, User.class).setParameter("id", id).getSingleResult();
		return admin;
	}

	@Override
	public String changePassword(User user) {
		String msg = "Password updation failed";
		User admin = findById(user.getUserId());
		if(admin != null) {
			admin.setPassword(user.getPassword());
			manager.merge(admin);
			msg = "password updated successfully";
		}
		return msg;
	}

	@Override
	public String generateReportAndAdoptionConfirmation(int childId) {
		return childrenDao.generateReportAndAdoptionConfirmation(childId);
	}

}
