package com.app.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;

import com.app.pojos.Adoptee;
import com.app.pojos.Child;
import com.app.pojos.Children;
import com.app.pojos.User;
import com.app.utils.PasswordUtils;

@Repository //spring bean : data access logic
public class AdopteeDaoImpl implements IAdopteeDao {
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	@Lazy
	private ChildrenDaoImpl childrenDao;
	
	@Autowired
    private JavaMailSender javaMailSender;

	@Override
	public Adoptee registerAdoptee(Adoptee adoptee) {
		String msg = "Adoptee's registration failed";
		
		//password encryption
		String saltPassword = PasswordUtils.getSalt(6);
		adoptee.setSaltPassword(saltPassword);
		String encryptPassword = PasswordUtils.generateSecurePassword(adoptee.getPassword(), saltPassword);
		adoptee.setPassword(encryptPassword);
		
		manager.persist(adoptee);
		msg = "Adoptee registered successfully";;
		sendEmail(adoptee.getEmail(), "Confirmation of registration", "Welcome to Child Adoption Portal\nYou have registered successfully as a Adoptee");
		return adoptee;
	}

	//@Autowired
    //private JavaMailSender javaMailSender;
    void sendEmail(String email, String subject, String body) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);
        msg.setSubject(subject);
        msg.setText(body);
        javaMailSender.send(msg);
    }

	@Override
	public Adoptee authenticateAdoptee(User user) {
		String msg = "Adoptee login failed";
		Adoptee a = null;
				
		//String jpql = "select a from Adoptee a  where a.email=:email and a.password=:pass";
		String jpql = "select a from Adoptee a  where a.email=:email";
		
		//a = manager.createQuery(jpql, Adoptee.class).setParameter("email", user.getEmail())
			//	.setParameter("pass", encryptPassword).setMaxResults(1).getSingleResult();
		
		a = manager.createQuery(jpql, Adoptee.class).setParameter("email", user.getEmail())
				.setMaxResults(1).getSingleResult();
		
		System.out.println("entity found : " + a);
		
		//password encryption
		String saltPassword = a.getSaltPassword();
		String encryptPassword = PasswordUtils.generateSecurePassword(user.getPassword(), saltPassword);
		
		if((a.getEmail().equals(user.getEmail())) && (a.getPassword().equals(encryptPassword))){
			msg = "Adoptee logged in successfully";
			return a;
		}
		else
			return null;
	}
	
	@Override
	public Adoptee findById(int id) {
		Adoptee a = null;
		String jpql = "select a from Adoptee a  where a.id=:id";
		a = manager.createQuery(jpql, Adoptee.class).setParameter("id", id).getSingleResult();
		return a;
	}

	@Override
	public String changePassword(User user) {
		String msg = "Password updation failed";
		Adoptee adoptee = findById(user.getUserId());
		if(adoptee != null) {
			adoptee.setPassword(user.getPassword());
			manager.merge(adoptee);
			msg = "password updated successfully";
		}
		return msg;
	}

	@Override
	public List<Children> getChildren() {
		List<Children> childrenList = new ArrayList<Children>();
		childrenList = childrenDao.getAllChildren();
		
		List<Children> newList = new ArrayList<Children>();
		for (Children children : childrenList) {
			if((!children.isAdopted()) && (children.getRequested() == null))
				newList.add(children);
		}
		return newList;
	}

	@Override
	public Children sendRequest(int childId, int adopteeId) {
		return childrenDao.requestForChild(childId, adopteeId);
	}

	@Override
	public Children viewChildProfile(int childId) {
		return childrenDao.findById(childId);
	}

	@Override
	public List<Adoptee> getAllAdoptees() {
		String jpql = "select a from Adoptee a";
		return manager.createQuery(jpql, Adoptee.class).getResultList(); 
	}

	@Override
	public Children confirmAdoption(int childId) {
		return childrenDao.confirmAdoption(childId);
	}

	@Override
	public List<Children> getRequestedChildren(int adopteeid) {
		return childrenDao.getRequestedChildren(adopteeid);
	}

	@Override
	public Children confirmRequest(int childId) {
		return childrenDao.confirmRequest(childId);
	}

	@Override
	public List<Children> getAdoptedChildrenByAdopteeId(int adopteeid) {
		return childrenDao.getAdoptedChildrenByAdopteeId(adopteeid);
	}
		
}
