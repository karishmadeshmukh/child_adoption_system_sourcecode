package com.app.dao;

import java.util.List;

import com.app.pojos.EducationalDetails;
import com.app.pojos.HealthStatus;

public interface IHealthStatusDao {
	//add method to get health status of all children
	List<HealthStatus> getHealthStatusOfAllChildren();
}
