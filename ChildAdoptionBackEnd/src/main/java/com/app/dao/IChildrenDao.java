package com.app.dao;

import java.util.List;

import com.app.pojos.ChildEntity;
import com.app.pojos.Children;

public interface IChildrenDao {
	//add a method to register new children
	String registerNewChildren(Children children);	//i/p : transient pojo

	//add a method to getAllChildren
	List<Children> getAllChildren();
	
	//add a method to get the children by id
	Children findById(int id);
	
	//add method to request a child
	Children requestForChild(int childId, int adopteeId);
	
	//add method to generate report and adoption confirmation
	String generateReportAndAdoptionConfirmation(int childId);
	
	//add method to add new children
	ChildEntity addNewChildren(Children children);
	
	//add method go get adopted children
	List<Children> getAdoptedChildren();
	
	//add method go get donated children
	List<Children> getDonatedChildrenList(int donorId);
	
	//add method to confirm adoption
	Children confirmAdoption(int childId);
	
	//add method to get requested children by specific adoptee
	List<Children> getRequestedChildren(int adopteeid);
	
	//add method to confirm request
	Children confirmRequest(int childId);
	
	//add method to confirm request
	Children acceptRequest(int childId);
	
	//add method to get adopted children by adoptee id
	List<Children> getAdoptedChildrenByAdopteeId(int adopteeid);
}
