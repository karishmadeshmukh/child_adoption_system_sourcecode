package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.app.pojos.Adoptee;
import com.app.pojos.HealthStatus;

@Repository //spring bean : data access logic
public class HealthStatusDaoImpl implements IHealthStatusDao{
	//javax.persistence.EntityManager (equivalent to org.hibernate.Session)
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<HealthStatus> getHealthStatusOfAllChildren() {
		String jpql = "select h from HealthStatus h";
		return manager.createQuery(jpql, HealthStatus.class).getResultList(); 
	}

}
