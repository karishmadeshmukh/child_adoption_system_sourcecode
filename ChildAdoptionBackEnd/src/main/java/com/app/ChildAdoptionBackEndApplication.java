package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages={"com.app"})
//@SpringBootApplication
@ComponentScan({ "com.app.controller","com.app.pojo","com.app.dao","com.app.pojos","com.app.service" })
public class ChildAdoptionBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChildAdoptionBackEndApplication.class, args);
	}
}

