package com.app.pojos;

public class Child {
	private int minAge;
	private int maxAge;
	private String gender;
	public Child() {
		System.out.println("Inside the ctor of Child");
	}
	public Child(int minAge, int maxAge, String gender) {
		super();
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.gender = gender;
	}
	public int getMinAge() {
		return minAge;
	}
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}
	public int getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child other = (Child) obj;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (maxAge != other.maxAge)
			return false;
		if (minAge != other.minAge)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Child [minAge=" + minAge + ", maxAge=" + maxAge + ", gender=" + gender + "]";
	}	
}
