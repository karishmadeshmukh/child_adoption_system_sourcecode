package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "educational_details")
public class EducationalDetails {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "edu_id")
	private int id;
	
	@Column(length = 100)
	private String degree;
	
	@Column(length = 100)
	private String institute;
	
	@OneToOne
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//@JsonIgnoreProperties(value = {"educational_details", "hibernateLazyInitializer", "handler"})
	//@SuppressWarnings("unchecked")
    @JoinColumn(name = "childId")
	private Children children;
	
	//def ctor : mandatory
	public EducationalDetails() {
		System.out.println("in EducationalDetails ctor");
	}
	//parameterized ctor
	public EducationalDetails(int id, String degree, String institute) {
		super();
		this.id = id;
		this.degree = degree;
		this.institute = institute;
	}
	//add all getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getInstitute() {
		return institute;
	}
	public void setInstitute(String institute) {
		this.institute = institute;
	}
	public Children getChildren() {
		return children;
	}
	public void setChildren(Children children) {
		this.children = children;
	}
	@Override
	public String toString() {
		return "EducationalDetails [id=" + id + ", degree=" + degree + ", institute=" + institute + "]";
	}
}
