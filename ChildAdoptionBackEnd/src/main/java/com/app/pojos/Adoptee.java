package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Adoptees")
public class Adoptee {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "adoptee_id")
	private int id;
	
	@Column(length = 255)
	private String password;
	
	@Column(length = 255)
	private String saltPassword;
	
	@Column(length = 100)
	private String name;
	
	@Column(length = 100, unique = true)
	private String email;
	
	@Column(length = 10)
	private String mobile;
	
	@Column(length = 10)
	private String gender;
	
	@Column(name = "marital_status", length = 255)
	private String maritalStatus;
	
	@Column(name = "no_of_biological_children")
	private int noOfBiologicalChildren;
	
	@Column(name = "no_of_adopted_children")
	private int noOfAdoptedChildren;
	
	@Column(length = 255)
	private String reason;
	
	@Column(length = 100)
	private String city;
	
	@Column(length = 100)
	private String state;
	
	@Column(length = 6)
	private String pincode;
	
	@Column(name = "annual_income")
	private float annualIncome;
	
	//def ctor : mandatory
	public Adoptee() {
		System.out.println("in Adoptee ctor");
	}
	//parameterized ctor
	public Adoptee(int id, String password, String saltPassword, String name, String email, String mobile, String gender,
			String maritalStatus, int noOfBiologicalChildren, int noOfAdoptedChildren, String reason,
			String city, String state, String pincode, float annualIncome) {
		super();
		this.id = id;
		this.password = password;
		this.saltPassword = saltPassword;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.noOfBiologicalChildren = noOfBiologicalChildren;
		this.noOfAdoptedChildren = noOfAdoptedChildren;
		this.reason = reason;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
		this.annualIncome = annualIncome;
	}
	//add all getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSaltPassword() {
		return saltPassword;
	}
	public void setSaltPassword(String saltPassword) {
		this.saltPassword = saltPassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public int getNoOfBiologicalChildren() {
		return noOfBiologicalChildren;
	}
	public void setNoOfBiologicalChildren(int noOfBiologicalChildren) {
		this.noOfBiologicalChildren = noOfBiologicalChildren;
	}
	public int getNoOfAdoptedChildren() {
		return noOfAdoptedChildren;
	}
	public void setNoOfAdoptedChildren(int noOfAdoptedChildren) {
		this.noOfAdoptedChildren = noOfAdoptedChildren;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public float getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(float annualIncome) {
		this.annualIncome = annualIncome;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adoptee other = (Adoptee) obj;
		if (Float.floatToIntBits(annualIncome) != Float.floatToIntBits(other.annualIncome))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id != other.id)
			return false;
		if (maritalStatus == null) {
			if (other.maritalStatus != null)
				return false;
		} else if (!maritalStatus.equals(other.maritalStatus))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (noOfAdoptedChildren != other.noOfAdoptedChildren)
			return false;
		if (noOfBiologicalChildren != other.noOfBiologicalChildren)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (pincode == null) {
			if (other.pincode != null)
				return false;
		} else if (!pincode.equals(other.pincode))
			return false;
		if (reason == null) {
			if (other.reason != null)
				return false;
		} else if (!reason.equals(other.reason))
			return false;
		if (saltPassword == null) {
			if (other.saltPassword != null)
				return false;
		} else if (!saltPassword.equals(other.saltPassword))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Adoptee [id=" + id + ", password=" + password + ", saltPassword=" + saltPassword + ", name=" + name
				+ ", email=" + email + ", mobile=" + mobile + ", gender=" + gender + ", maritalStatus=" + maritalStatus
				+ ", noOfBiologicalChildren=" + noOfBiologicalChildren
				+ ", noOfAdoptedChildren=" + noOfAdoptedChildren + ", reason=" + reason + ", city=" + city + ", state="
				+ state + ", pincode=" + pincode + ", annualIncome=" + annualIncome + "]";
	}
}
