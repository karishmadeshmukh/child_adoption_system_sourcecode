package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "children")
public class Children {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "child_id")
	private Integer childId;
	
	@Column(length = 30)
	private String name;
	
	@Column(name = "dob")
    private LocalDate birthDate;
	
	private Integer age;
	
	@Column(length = 30)
	private String gender;
	
	@Column(length = 555)
	private String image;
	
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//@JsonIgnoreProperties(value = {"children", "hibernateLazyInitializer", "handler"})
	//@SuppressWarnings("unchecked")
	@OneToOne(mappedBy = "children", cascade = CascadeType.ALL)
	private HealthStatus healthStatus;
	
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//@JsonIgnoreProperties(value = {"children", "hibernateLazyInitializer", "handler"})
	//@SuppressWarnings("unchecked")
	@OneToOne(mappedBy = "children", cascade = CascadeType.ALL)
	private EducationalDetails educationalDetails;
	
	//bi dirrectional association between entities
	//many side of the association and owning side (since it has FK column)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "donor_id",nullable = true)//constraint : NOT NULL : Optional BUT recommended
	private Donor donor;
	
	//bi dirrectional association between entities
	//many side of the association and owning side (since it has FK column)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "adoptee_id",nullable = true)//constraint : NOT NULL : Optional BUT recommended
	private Adoptee adoptee;
	
	private Integer requested;
	private Integer reqaccepted;
	private Integer confirmed;

	@Column(columnDefinition = "boolean default false")
	private boolean isAdopted = false;
	
	@Column(columnDefinition = "boolean default false")
	private boolean report = false;
	
	//def ctor : mandatory
	public Children() {
		System.out.println("in Children ctor");
	}
	//add parametrized constr
	public Children(Integer childId, String name, LocalDate birthDate, Integer age, String gender, String image,
			HealthStatus healthStatus, EducationalDetails educationalDetails, Donor donor, Adoptee adoptee,
			Integer requested, Integer reqaccepted, Integer confirmed, boolean isAdopted, boolean report) {
		super();
		this.childId = childId;
		this.name = name;
		this.birthDate = birthDate;
		this.age = age;
		this.gender = gender;
		this.image = image;
		this.healthStatus = healthStatus;
		this.educationalDetails = educationalDetails;
		this.donor = donor;
		this.adoptee = adoptee;
		this.requested = requested;
		this.reqaccepted = reqaccepted;
		this.confirmed = confirmed;
		this.isAdopted = isAdopted;
		this.report = report;
	}
	//add all getters and setters
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public HealthStatus getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(HealthStatus healthStatus) {
		this.healthStatus = healthStatus;
	}
	public EducationalDetails getEducationalDetails() {
		return educationalDetails;
	}
	public void setEducationalDetails(EducationalDetails educationalDetails) {
		this.educationalDetails = educationalDetails;
	}
	public Donor getDonor() {
		return donor;
	}
	public void setDonor(Donor donor) {
		this.donor = donor;
	}
	public Adoptee getAdoptee() {
		return adoptee;
	}
	public void setAdoptee(Adoptee adoptee) {
		this.adoptee = adoptee;
	}
	public boolean isAdopted() {
		return isAdopted;
	}
	public void setAdopted(boolean isAdopted) {
		this.isAdopted = isAdopted;
	}
	public boolean isReport() {
		return report;
	}
	public void setReport(boolean report) {
		this.report = report;
	}
	public Integer getRequested() {
		return requested;
	}
	public void setRequested(Integer requested) {
		this.requested = requested;
	}
	public Integer getReqaccepted() {
		return reqaccepted;
	}
	public void setReqaccepted(Integer reqaccepted) {
		this.reqaccepted = reqaccepted;
	}
	public Integer getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Integer confirmed) {
		this.confirmed = confirmed;
	}
	@Override
	public String toString() {
		return "Children [childId=" + childId + ", name=" + name + ", birthDate=" + birthDate + ", age=" + age
				+ ", gender=" + gender + ", image=" + image + ", isAdopted=" + isAdopted + ", report=" + report + " requested=" + requested + "]";
	}
}
