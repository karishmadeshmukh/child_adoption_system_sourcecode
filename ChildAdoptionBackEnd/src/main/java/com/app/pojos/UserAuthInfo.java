package com.app.pojos;

public class UserAuthInfo {
	private String email;
	private String password;
	
	public UserAuthInfo() {
		System.out.println("Inside UserAuthInfo ctor");
	}
	public UserAuthInfo(String email, String password) {
		this.email = email;
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserAuthInfo [email=" + email + ", password=" + password + "]";
	}
}
