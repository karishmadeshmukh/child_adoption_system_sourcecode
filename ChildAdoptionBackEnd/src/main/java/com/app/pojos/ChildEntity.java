package com.app.pojos;

import java.time.LocalDate;

public class ChildEntity {
	private String name;
	private LocalDate birthDate;
	private int age;
	private String gender;
	private String image;
	private boolean isAdopted;
	private boolean report;
	private float weight;
	private float height;
	private String bloodGroup;
	private float hb;
	private String mentalHealthStatus;
	private String physicalHealthStatus;
	private String degree;
	private String institute;
	
	public ChildEntity() {
		System.out.println("ChildEntity ctor called");
	}

	public ChildEntity(String name, LocalDate birthDate, int age, String gender, String image, boolean isAdopted,
			boolean report, float weight, float height, String bloodGroup, float hb, String mentalHealthStatus,
			String physicalHealthStatus, String degree, String institute) {
		super();
		this.name = name;
		this.birthDate = birthDate;
		this.age = age;
		this.gender = gender;
		this.image = image;
		this.isAdopted = isAdopted;
		this.report = report;
		this.weight = weight;
		this.height = height;
		this.bloodGroup = bloodGroup;
		this.hb = hb;
		this.mentalHealthStatus = mentalHealthStatus;
		this.physicalHealthStatus = physicalHealthStatus;
		this.degree = degree;
		this.institute = institute;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isAdopted() {
		return isAdopted;
	}

	public void setAdopted(boolean isAdopted) {
		this.isAdopted = isAdopted;
	}

	public boolean isReport() {
		return report;
	}

	public void setReport(boolean report) {
		this.report = report;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public float getHb() {
		return hb;
	}

	public void setHb(float hb) {
		this.hb = hb;
	}

	public String getMentalHealthStatus() {
		return mentalHealthStatus;
	}

	public void setMentalHealthStatus(String mentalHealthStatus) {
		this.mentalHealthStatus = mentalHealthStatus;
	}

	public String getPhysicalHealthStatus() {
		return physicalHealthStatus;
	}

	public void setPhysicalHealthStatus(String physicalHealthStatus) {
		this.physicalHealthStatus = physicalHealthStatus;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getInstitute() {
		return institute;
	}

	public void setInstitute(String institute) {
		this.institute = institute;
	}

	@Override
	public String toString() {
		return "ChildEntity [name=" + name + ", birthDate=" + birthDate + ", age=" + age + ", gender=" + gender
				+ ", image=" + image + ", isAdopted=" + isAdopted + ", report=" + report + ", weight=" + weight
				+ ", height=" + height + ", bloodGroup=" + bloodGroup + ", hb=" + hb + ", mentalHealthStatus="
				+ mentalHealthStatus + ", physicalHealthStatus=" + physicalHealthStatus + ", degree=" + degree
				+ ", institute=" + institute + "]";
	}
}
