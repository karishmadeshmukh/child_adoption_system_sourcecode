package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "donors")
public class Donor {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "donor_id")
	private int id;
	
	@Column(length = 255)
	private String password;
	
	@Column(length = 255)
	private String saltPassword;
	
	@Column(length = 100)
	private String name;
	
	@Column(length = 100, unique = true)
	private String email;
	
	@Column(length = 10)
	private String mobile;
	
	@Column(length = 10)
	private String gender;
	
	@Column(length = 100)
	private String city;
	
	@Column(length = 100)
	private String state;
	
	@Column(length = 6)
	private String pincode;
	
	@Column(name = "relation_with_child", length = 100)
	private String relation;
	
	@Column(length = 255)
	private String reason;
	
	//def ctor : mandatory
	public Donor() {
		System.out.println("in Donor ctor");
	}
	public Donor(int id, String password, String saltPassword, String name, String email, String mobile, String gender,
			String city, String state, String pincode, String relation, String reason) {
		super();
		this.id = id;
		this.password = password;
		this.saltPassword = saltPassword;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
		this.relation = relation;
		this.reason = reason;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSaltPassword() {
		return saltPassword;
	}
	public void setSaltPassword(String saltPassword) {
		this.saltPassword = saltPassword;
	}
	@Override
	public String toString() {
		return "Donor [id=" + id + ", password=" + password + ", saltPassword=" + saltPassword + ", name=" + name
				+ ", email=" + email + ", mobile=" + mobile + ", gender=" + gender + ", city="
				+ city + ", state=" + state + ", pincode=" + pincode + ", relation=" + relation + ", reason=" + reason
				+ "]";
	}
}
