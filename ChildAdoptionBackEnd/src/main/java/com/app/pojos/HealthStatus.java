package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "health_status")
public class HealthStatus {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "health_id")
	private int id;
	
	private float weight;
	
	private float height;
	
	@Column(name = "blood_group", length = 5)
	private String bloodGroup;
	
	private float hb;
	
	@Column(name = "mental_health", length = 255)
	private String mentalHealthStatus;
	
	@Column(name = "physical_health", length = 255)
	private String physicalHealthStatus;
	
	@OneToOne
	@JsonIgnore
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//@JsonIgnoreProperties(value = {"health_status", "hibernateLazyInitializer", "handler"})
	//@SuppressWarnings("unchecked")
    @JoinColumn(name = "childId")
	private Children children;
	
	//def ctor : mandatory
	public HealthStatus() {
		System.out.println("in HealthStatus ctor");
	}
	//parameterized ctor
	public HealthStatus(int id, float weight, float height, String bloodGroup, float hb, String mentalHealthStatus,
			String physicalHealthStatus) {
		super();
		this.id = id;
		this.weight = weight;
		this.height = height;
		this.bloodGroup = bloodGroup;
		this.hb = hb;
		this.mentalHealthStatus = mentalHealthStatus;
		this.physicalHealthStatus = physicalHealthStatus;
	}
	//add all getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public float getHb() {
		return hb;
	}
	public void setHb(float hb) {
		this.hb = hb;
	}
	public String getMentalHealthStatus() {
		return mentalHealthStatus;
	}
	public void setMentalHealthStatus(String mentalHealthStatus) {
		this.mentalHealthStatus = mentalHealthStatus;
	}
	public String getPhysicalHealthStatus() {
		return physicalHealthStatus;
	}
	public void setPhysicalHealthStatus(String physicalHealthStatus) {
		this.physicalHealthStatus = physicalHealthStatus;
	}
	public Children getChildren() {
		return children;
	}
	public void setChildren(Children children) {
		this.children = children;
	}
	@Override
	public String toString() {
		return "HealthStatus [id=" + id + ", weight=" + weight + ", height=" + height + ", bloodGroup=" + bloodGroup
				+ ", hb=" + hb + ", mentalHealthStatus=" + mentalHealthStatus + ", physicalHealthStatus="
				+ physicalHealthStatus + "]";
	}	
}
