package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "request")
public class Request {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //auto_increment
	@Column(name = "req_id")
	private Integer reqId;
	
	@Column(name = "child_id")
	private Integer childId;
	
	@Column(name = "adoptee_id")
	private Integer adopteeId;
	
	public Request() {
		System.out.println("Inside the ctor of Request()");
	}
	public Request(Integer reqId, Integer childId, Integer adopteeId) {
		super();
		this.reqId = reqId;
		this.childId = childId;
		this.adopteeId = adopteeId;
	}
	public Integer getReqId() {
		return reqId;
	}
	public void setReqId(Integer reqId) {
		this.reqId = reqId;
	}
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getAdopteeId() {
		return adopteeId;
	}
	public void setAdopteeId(Integer adopteeId) {
		this.adopteeId = adopteeId;
	}
	@Override
	public String toString() {
		return "Request [reqId=" + reqId + ", childId=" + childId + ", adopteeId=" + adopteeId + "]";
	}
}
